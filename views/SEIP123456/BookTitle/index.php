<?php
require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
use App\Message\Message;
$objBookTitle = new BookTitle();
$allData = $objBookTitle->index("obj");

$serial=1;

######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBookTitle->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################




?>






<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>



</head>

<body>


    <table>
        <tr>
            <td width="500">
                <h2>Active List of Book Titles</h2>
            </td>
            <td width="450">
                <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
                <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
                <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>
            </td>
            <td width="150">
              
              

            
            </td>
        </tr>
    </table>



<div id="TopMenuBar">
    <button type="button" onclick="window.location.href='../index.php'" class=" btn-primary btn-lg">Home</button>
    <button type="button" onclick="window.location.href='create.php'" class=" btn-primary btn-lg">Add new</button>
    <button type="button" onclick="window.location.href='trashed.php?Page=1'" class=" btn-success btn-lg">Trashed List</button>
</div>

<h1> Active List</h1>

<?php


echo "<table border='5px' >";

echo "<th> Serial </th>";
echo "<th> ID </th>";
echo "<th> Book Title </th>";
echo "<th> Author Name </th>";
echo "<th> Action </th>";


foreach($someData as $oneData)//somedata is required for pagination
{
   echo "<tr style='height: 40px'>";
    echo "<td>".$serial."</td>";

    echo "<td>".$oneData->id."</td>";
    echo "<td>".$oneData->book_title."</td>";
    echo "<td>".$oneData->author_name."</td>";


    echo "<td>";

    echo "<a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a> ";
    echo "<a href='edit.php?id=$oneData->id'><button class='btn btn-primary'>Edit</button></a> ";
    echo "<a href='trash.php?id=$oneData->id'><button class='btn btn-success'>Trash</button></a> ";

    echo "<a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a> ";


    echo "</td>";

    echo "</tr>";

    $serial++;
}

echo "</table>";

?>



</body>
</html>


